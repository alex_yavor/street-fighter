class ModalWindow {
    constructor() { }

    static form = document.getElementById('prompt-form');
    static container = document.getElementById('prompt-form-container');
    static document = document;

    openWindow(fightersMap, id) {
        const fighterDetails = fightersMap.get(id);
        const { name, attack, defense, health, source } = fighterDetails;
        let form = this.constructor.form;
        let elements = form.elements;

        elements.name.value = name;
        elements.attack.value = attack;
        elements.defense.value = defense;
        elements.health.value = health;

        this.constructor.showCover();
        this.constructor.container.style.display = 'block';
        form.elements.submit.onclick = () => this.constructor.saveChanges(elements, fightersMap, source, id);
        form.elements.cancel.onclick = () => this.constructor.complete();
    }

    static saveChanges(elements, fightersMap, source, _id) {
        const name = elements.name.value;
        const attack = elements.attack.value;
        const defense = elements.defense.value;
        const health = elements.health.value;
        if(attack<1){
            alert("Attack must be greater than 0");
            return;
        }
        if(defense<0){
            alert("Defense cannot be negative");
            return;
        }
        if(health<1){
            alert("Health must be greater than 0");
            return;
        }
        const fightersDiv = document.getElementsByClassName('fighter');

        fightersDiv[_id - 1].lastElementChild.innerHTML = name;
        fightersMap.set(_id, {
            name,
            attack,
            defense,
            health,
            source,
            _id
        })

        this.complete();
    }

    static showCover() {
        let coverDiv = document.createElement('div');
        coverDiv.id = 'cover-div';
        document.body.appendChild(coverDiv);
    }

    static hideCover() {
        let coverDiv = document.getElementById('cover-div');
        document.body.removeChild(coverDiv);
    }

    static complete() {
        this.hideCover();
        this.container.style.display = 'none';
    }
}

export let modalWindow = new ModalWindow();