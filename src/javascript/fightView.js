
import View from "./view";

export default class FightView extends View {
    constructor(fighters) {
        super();
        this.firstFighter = fighters.firstFighter;
        this.secondFighter = fighters.secondFighter;
        this.createFight();
        this.beginFight();
    }

    createFight() {

        this.firstFighterHealthView = this.createHealthBar(this.firstFighter.health);
        this.secondFighterHealthView = this.createHealthBar(this.secondFighter.health);

        this.firstFighterImage = this.createImage({
            src: this.firstFighter.source
        });
        this.secondFighterImage = this.createImage({
            src: this.secondFighter.source,
            style: "transform: scale(-1, 1);"
        });
        this.chooseFighters = this.createElement({
            tagName: "button",
            className: 'btn',
            attributes: {
                style: "display:none;"
            }
        })
        this.chooseFighters.innerHTML = 'Select new fighters';
        this.chooseFighters.addEventListener('click',()=>location.reload())

        this.element = this.createElement({ tagName: 'div', className: 'fighters', attributes: { class: "battle fighters" } });
        this.element.append(
            this.firstFighterHealthView,
            this.secondFighterHealthView,
            this.firstFighterImage,
            this.secondFighterImage,
            this.chooseFighters);
    }

    createHealthBar(health) {
        const attributes = { max: `${health}`, value: `${health}` };
        const healthBar = this.createElement({
            tagName: 'progress',
            className: 'progress-bar',
            attributes
        })

        return healthBar;
    }

    createImage(attributes) {
        const imgElement = this.createElement({
            tagName: 'img',
            className: 'fighter-image',
            attributes
        });

        return imgElement;
    }

    beginFight() {
        const attackPeriod = 500; //ms
        let timer = setInterval(() => {
            this.firstFighter.health = this.reduceHealth(this.secondFighter, this.firstFighter);
            this.firstFighterHealthView.setAttribute('value', this.firstFighter.health);
            if (this.firstFighter.health <= 0) { this.endGame(this.secondFighter.name); clearInterval(timer); }
            this.secondFighter.health = this.reduceHealth(this.firstFighter, this.secondFighter);
            this.secondFighterHealthView.setAttribute('value', this.secondFighter.health)
            if (this.secondFighter.health <= 0) { this.endGame(this.firstFighter.name); clearInterval(timer); }
        }, attackPeriod)
    }

    reduceHealth(attacker, defender) {
        let reduceBy = attacker.getHitPower() - defender.getBlockPower();
        if (reduceBy < 0) {
            reduceBy = 0
        };
        const health = defender.health - reduceBy;

        return health;
    }

    endGame(winner) {
        alert(`${winner} won!`);
        this.chooseFighters.style.display = "inline-block";
        this.firstFighterImage.style.display = 'none';
        this.secondFighterImage.style.display = 'none';
    }

}