import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import { modalWindow } from './modalWindow';
import fight from './fight';
import Fighter from './fighter';

class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
    const fightButton = document.getElementById('fight');
    fightButton.addEventListener('click', (event) => this.prepareToFight(event), false);
    fightButton.style.display = 'block';
  }

  async prepareToFight(event) {
    let checks = document.getElementsByClassName('chooseFighter');
    let fightersId = [];
    [].forEach.call(checks, (item) => {
      if (item.checked) {
        fightersId.push(item.id);
      }
    })
    if (fightersId.length != 2) {
      alert('Choose 2 fighters')
    }
    else {
      const firstId = fightersId[0], secondId = fightersId[1];
      if (!this.fightersDetailsMap.has(firstId)) await this.setFighterDetails(firstId);
      if (!this.fightersDetailsMap.has(secondId)) await this.setFighterDetails(secondId);
      let firstFighter = new Fighter(this.fightersDetailsMap.get(firstId));
      let secondFighter = new Fighter(this.fightersDetailsMap.get(secondId));
      fight({ firstFighter, secondFighter });
    }
  }

  async handleFighterClick(event, fighter) {
    if (event.target.tagName == 'INPUT') { return };
    let id = fighter._id;
    if (!this.fightersDetailsMap.has(id)) {
      await this.setFighterDetails(id);
    }
    await modalWindow.openWindow(this.fightersDetailsMap, id);
  }
  
  async setFighterDetails(id) {
    const fighterDetails = await fighterService.getFighterDetails(id);
    this.fightersDetailsMap.set(id, fighterDetails);
  }
}

export default FightersView;