export default class Fighter {

    constructor(details) {

        this.name = details.name;
        this.health = details.health;
        this.attack = details.attack;
        this.defense = details.defense;
        this.source = details.source;
        this._id = details._id;
    }

    get criticalHitChance() {
        const max = 2, min = 1;

        return Math.floor((Math.random() * max) + min)
    }

    get dodgeChance() {
        const max = 2, min = 1;

        return Math.floor((Math.random() * max) + min)
    }

    getHitPower() {
        return this.attack * this.criticalHitChance;
    }

    getBlockPower() {
        return this.defense * this.dodgeChance;
    }
}
