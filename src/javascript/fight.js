import FightView from "./fightView";

export default function fight(fighters) {
    // console.log(firstFighter, secondFighter);
    const fightView = new FightView(fighters);
    const rootElement = document.getElementById('root');
    while (rootElement.firstChild) {
        rootElement.firstChild.remove();
    }
    rootElement.appendChild(fightView.element);

}